package br.com.managersistems.pais.service;

import br.com.managersistems.pais.domain.Pais;
import br.com.managersistems.pais.repository.PaisRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class PaisService {

    private final Logger log = LoggerFactory.getLogger(PaisService.class);

    private final PaisRepository _paisRepository;

    public PaisService(PaisRepository paisRepository) {
        this._paisRepository = paisRepository;
    }

    public List<Pais> listar() {
        return _paisRepository.findAll();
    }

    public Pais salvar(Pais paisIn) {
        return _paisRepository.save(paisIn);
    }

    public List<Pais> pesquisar(String filtroIn) {
        if (filtroIn == null) {
            return _paisRepository.findByFilter("");
        }
        return _paisRepository.findByFilter(filtroIn.toUpperCase());
    }

    public Boolean excluir(Long idIn) {
        if (_paisRepository.existsById(idIn)) {
            _paisRepository.deleteById(idIn);
            return true;
        }
        return false;
    }

}
