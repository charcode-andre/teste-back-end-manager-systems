package br.com.managersistems.pais.service;

import br.com.managersistems.pais.domain.Token;
import br.com.managersistems.pais.domain.Usuario;
import br.com.managersistems.pais.exception.BadRequestException;
import br.com.managersistems.pais.repository.TokenRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class TokenService {

    private final Logger log = LoggerFactory.getLogger(TokenService.class);

    private final TokenRepository _tokenRepository;

    public TokenService(TokenRepository tokenRepository) {
        this._tokenRepository = tokenRepository;
    }

    public Token criar(Usuario usuario) {
        List<Token> tokens = _tokenRepository.findAllByUsuario_Id(usuario.getId());
        _tokenRepository.deleteAll(tokens);
        Token token = new Token(usuario);
        return _tokenRepository.save(token);
    }

    public Boolean renovar(String tokenIn) {
        Token token = _tokenRepository.findOneByToken(tokenIn);
        if(token != null) {
            token.renovar();
            _tokenRepository.save(token);
            return true;
        } else {
            return false;
        }
    }

    public Token findbyToken(String tokenIn) {
        return _tokenRepository.findOneByToken(tokenIn);
    }
}
