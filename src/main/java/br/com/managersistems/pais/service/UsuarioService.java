package br.com.managersistems.pais.service;

import br.com.managersistems.pais.domain.Token;
import br.com.managersistems.pais.domain.Usuario;
import br.com.managersistems.pais.exception.BadRequestException;
import br.com.managersistems.pais.repository.UsuarioRepository;
import br.com.managersistems.pais.web.rest.dto.UsuarioAutenticado;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class UsuarioService {

    private final Logger log = LoggerFactory.getLogger(UsuarioService.class);

    private final UsuarioRepository _usuarioRepository;

    private final TokenService _tokenService;

    public UsuarioService(UsuarioRepository usuarioRepository, TokenService tokenService) {
        this._usuarioRepository = usuarioRepository;
        this._tokenService = tokenService;
    }

    public UsuarioAutenticado autenticar(String login, String senha) {
        Usuario usuario =_usuarioRepository.findByLoginAndSenha(login, senha);
        if (usuario == null) {
            throw new BadRequestException("Credenciais invalidas");
        }
        Token toke = _tokenService.criar(usuario);
        return new UsuarioAutenticado(usuario, toke);
    }

    public Boolean renovarTicket(String tokenIn) {
        return  _tokenService.renovar(tokenIn);
    }

}
