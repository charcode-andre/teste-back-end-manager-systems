package br.com.managersistems.pais.repository;

import br.com.managersistems.pais.domain.Token;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.ZonedDateTime;
import java.util.List;

@Repository
public interface TokenRepository extends JpaRepository<Token, Long> {

    @Modifying
    @Query(" UPDATE Token t SET t.expiracao = :expiracao WHERE t.token = :token")
    void renovar(@Param("token") String token, @Param("expiracao") ZonedDateTime expiracao);

    Token findOneByToken(String token);

    List<Token> findAllByUsuario_Id(Long id);
}
