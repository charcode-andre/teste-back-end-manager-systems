package br.com.managersistems.pais.repository;

import br.com.managersistems.pais.domain.Pais;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PaisRepository extends JpaRepository<Pais, Long> {

    @Query("SELECT p FROM Pais p WHERE UPPER(p.nome) LIKE %:filter% " +
            "OR UPPER(p.sigla) LIKE %:filter% " +
            "OR UPPER(p.gentilico) LIKE %:filter%")
    List<Pais> findByFilter(@Param("filter") String filtro);
}