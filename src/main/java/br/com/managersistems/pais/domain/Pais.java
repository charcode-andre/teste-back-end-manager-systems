package br.com.managersistems.pais.domain;

import javax.persistence.*;

@Entity
@Table(name = "pais")
public class Pais {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_PAIS")
    @SequenceGenerator(name = "SEQ_PAIS", sequenceName = "SEQ_PAIS", allocationSize = 1, initialValue = 4)
    private Long id;
    private String nome;
    private String sigla;
    private String gentilico;

    protected Pais() {
    }

    public Pais(String gentilico, String nome, String sigla) {
        this.gentilico = gentilico;
        this.nome = nome;
        this.sigla = sigla;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        if (id == 0L) {
            this.id = null;
        }
        this.id = id;
    }

    public String getGentilico() {
        return gentilico;
    }

    public void setGentilico(String gentilico) {
        this.gentilico = gentilico;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSigla() {
        return sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    @Override
    public String toString() {
        return "Pais{" +
                "id=" + id +
                ", gentilico='" + gentilico + '\'' +
                ", nome='" + nome + '\'' +
                ", sigla='" + sigla + '\'' +
                '}';
    }

}
