package br.com.managersistems.pais.domain;

import javax.persistence.*;
import java.time.ZonedDateTime;
import java.util.UUID;

@Entity
@Table(name = "token")
public class Token {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_TOKEN")
    @SequenceGenerator(name = "SEQ_TOKEN", sequenceName = "SEQ_TOKEN", allocationSize = 1)
    private Long id;
    private String token;
    private ZonedDateTime expiracao;
    private Boolean administrador;
    @ManyToOne
    private Usuario usuario;

    public Token() {
    }

    public Token(Usuario usuario) {
        this.token = UUID.randomUUID().toString();
        this.expiracao = ZonedDateTime.now().plusMinutes(5);
        this.administrador = usuario.getAdministrador();
        this.usuario = usuario;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public ZonedDateTime getExpiracao() {
        return expiracao;
    }

    public void setExpiracao(ZonedDateTime expiracao) {
        this.expiracao = expiracao;
    }

    public Boolean getAdministrador() {
        return administrador;
    }

    public void setAdministrador(Boolean administrador) {
        this.administrador = administrador;
    }

    public void renovar() {
        this.expiracao = ZonedDateTime.now().plusMinutes(5);
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    @Override
    public String toString() {
        return "Token{" +
                "id=" + id +
                ", token='" + token + '\'' +
                ", expiracao=" + expiracao +
                ", administrador=" + administrador +
                '}';
    }
}
