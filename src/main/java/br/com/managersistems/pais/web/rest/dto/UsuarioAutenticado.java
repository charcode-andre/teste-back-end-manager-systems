package br.com.managersistems.pais.web.rest.dto;

import br.com.managersistems.pais.domain.Token;
import br.com.managersistems.pais.domain.Usuario;

public class UsuarioAutenticado {

    private String login;
    private String nome;
    private String token;
    private Boolean administrador;
    private Boolean autenticado;

    public UsuarioAutenticado(Usuario usuario, Token token) {
        this.login = usuario.getLogin();
        this.nome = usuario.getNome();
        this.token = token.getToken();
        this.administrador = usuario.getAdministrador();
        this.autenticado = true;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Boolean getAdministrador() {
        return administrador;
    }

    public void setAdministrador(Boolean administrador) {
        this.administrador = administrador;
    }

    public Boolean getAutenticado() {
        return autenticado;
    }

    public void setAutenticado(Boolean autenticado) {
        this.autenticado = autenticado;
    }
}
