package br.com.managersistems.pais.web.rest;

import br.com.managersistems.pais.domain.Pais;
import br.com.managersistems.pais.service.PaisService;
import br.com.managersistems.pais.service.UsuarioService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/pais")
@Api(value = "País")
public class PaisResource {

    private final Logger log = LoggerFactory.getLogger(PaisResource.class);

    private PaisService _paisService;

    private UsuarioService _usuarioService;

    public PaisResource(PaisService paisService, UsuarioService usuarioService) {
        this._paisService = paisService;
        this._usuarioService = usuarioService;
    }

    @ApiOperation(value = "Lista os países cadastrados",
            notes = "Lista dos países ou HTTP_ERROR 401 se não autenticado")
    @GetMapping(value = "/listar", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<List<Pais>> listar(@RequestParam String token) {
        return ResponseEntity.ok(_paisService.listar());
    }

    @ApiOperation(value = "Inclui/altera um país cadastrado",
            notes = "Se recebe um Pais com id 0, inclui um novo país, caso contrário altera o país com id informado. " +
                    "O objeto pais salvo atualizado ou HTTP_ERROR 401 se não autenticado ou o usuário não é " +
                    "administrador")
    @PostMapping(value = "/salvar", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Pais> salvar(@RequestParam String token, @RequestBody(required = false) Pais pais) {
        return ResponseEntity.ok(_paisService.salvar(pais));
    }

    @ApiOperation(value = "Retorna os países cujo nome contém o texto informado",
            notes = "Case insensitive. Lista dos países ou HTTP_ERROR 401 se não autenticado")
    @GetMapping(value = "/pesquisar", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiParam(value="Token de autenticação gerado anteriormente", name="token")
    public ResponseEntity<List<Pais>> pesquisar(@RequestParam(name = "token") String token, String filtro) {
        return ResponseEntity.ok(_paisService.pesquisar(filtro));
    }

    @ApiOperation(value = "Remove o pais de id informado",
            notes = "true se excluiu, false se não encontrou ou HTTP_ERROR 401 se não autenticado ou " +
                    "o usuário não é administrador")
    @GetMapping(value = "/excluir", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Boolean> excluir(@RequestParam String token, @RequestParam Long id) {
        return ResponseEntity.ok(_paisService.excluir(id));
    }
}
