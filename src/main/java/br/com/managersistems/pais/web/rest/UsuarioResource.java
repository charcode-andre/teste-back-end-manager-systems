package br.com.managersistems.pais.web.rest;

import br.com.managersistems.pais.service.UsuarioService;
import br.com.managersistems.pais.web.rest.dto.UsuarioAutenticado;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/usuario")
public class UsuarioResource {

    private final Logger log = LoggerFactory.getLogger(UsuarioResource.class);

    private UsuarioService _usuarioService;

    public UsuarioResource(UsuarioService usuarioService) {
        this._usuarioService = usuarioService;
    }

    @ApiOperation(value = "Autentica o usuário", notes = "Consulta a tabela de usuários por um registro com os dados " +
            "passados. Objeto UsuarioAutenticado indicando se o usuário foi autenticado, e em caso positivo, com o " +
            "token gerado na autenticação, incluindo também a informação se o usuário é administrador.")
    @PostMapping(value = "/autenticar", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<UsuarioAutenticado> autenticar(@RequestParam String login, @RequestParam String senha) {
        return ResponseEntity.ok(_usuarioService.autenticar(login, senha));
    }

    @ApiOperation(value = "Renova um ticket gerado anteriormente", notes = "true se reautenticou o token, false se não " +
            "conseguiu reautenticar (token não existe, por exemplo)")
    @GetMapping(value = "/renovar-ticket", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Boolean> renovarTicket(@RequestParam String token) {
        return ResponseEntity.ok(_usuarioService.renovarTicket(token));
    }
}
