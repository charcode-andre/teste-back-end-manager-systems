package br.com.managersistems.pais;

import java.io.IOException;
import java.time.ZonedDateTime;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.managersistems.pais.domain.Token;
import br.com.managersistems.pais.service.TokenService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(1)
public class TransactionFilter extends GenericFilter {

    private final Logger log = LoggerFactory.getLogger(TransactionFilter.class);

    @Autowired
    private TokenService _tokenService;

    @Override
    public void doFilter(
            ServletRequest request,
            ServletResponse response,
            FilterChain chain) throws IOException, ServletException {

        HttpServletRequest req = (HttpServletRequest) request;

        if(req.getRequestURI().matches("/pais/.*")) {
            Token token = _tokenService.findbyToken(req.getParameter("token"));
            if(token == null
                || token.getExpiracao().isBefore(ZonedDateTime.now())
                || (req.getRequestURI().matches(".*/excluir") && !token.getAdministrador())
                || (req.getRequestURI().matches(".*/salvar") && !token.getAdministrador())) {
                ((HttpServletResponse) response).setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                ((HttpServletResponse) response).sendError(HttpServletResponse.SC_UNAUTHORIZED);
                return;
            }
        }
        chain.doFilter(request, response);
    }
}
