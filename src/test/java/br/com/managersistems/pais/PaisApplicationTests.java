package br.com.managersistems.pais;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = PaisApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class PaisApplicationTests {

    @Autowired
    private TestRestTemplate _restTemplate;

    private static HttpHeaders _headers;

    @LocalServerPort
    private Integer port;

    private static String _url;
    private static String _paisUrl;
    private static String _usuarioUrl;

    private static Map<String, Object> _loginConvidadoJsonObject;
    private static Map<String, Object> _loginAdminJsonObject;

    private final ObjectMapper objectMapper = new ObjectMapper();

    @BeforeClass
    public static void runBeforeAllTestMethods() {
        _url = "http://localhost:#PORT";
	    _paisUrl = _url + "/pais";
	    _usuarioUrl = _url + "/usuario";

	    _headers = new HttpHeaders();
	    //_headers.setContentType(MediaType.APPLICATION_JSON);

        _loginConvidadoJsonObject = new HashMap<>();
        _loginConvidadoJsonObject.put("login", "convidado");
        _loginConvidadoJsonObject.put("senha", "manager");

        _loginAdminJsonObject = new HashMap<>();
        _loginAdminJsonObject.put("login", "admin");
        _loginAdminJsonObject.put("senha", "suporte");

    }

    @Test
    public void autenticarConvidadoTest() throws IOException {
        HttpEntity request = new HttpEntity(this._loginConvidadoJsonObject, _headers);
        String url = _usuarioUrl.replace("#PORT", port.toString()) + "/autenticar";
        String response = _restTemplate.postForObject(url, request, String.class);
        JsonNode root = objectMapper.readTree(response);
        Assert.assertNotNull(root);

    }

}
