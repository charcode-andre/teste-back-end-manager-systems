# Teste - Back End - Manager Systems

## Instalação
$ mvn install

## Inicialização da aplicação
$ mvn spring-boot:run

## Acesso Swagger
Local: http://localhost:8088/swagger-ui.html

OU

Aplicação na AWS: http://ec2-18-234-191-19.compute-1.amazonaws.com:8088/swagger-ui.html

